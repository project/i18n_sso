/**
 * @file
 * Loaded on 403 pages to check if user is logged in on main language domain.
 *
 * If so, it logs in user on current website and reload page.
 */

((Drupal, drupalSettings, once) => {
  Drupal.behaviors.i18n_sso = Drupal.behaviors.i18n_sso || {};
  Drupal.behaviors.i18n_sso.attach = (context) => {
    Drupal.behaviors.i18n_sso.initLogin(context);
  };

  /**
   * Calls get-token url on main domain (domain of the main language).
   *
   * Gets a token if user is logged in and then uses it on local domain to log
   * user in.
   *
   * @param {HTMLDocument|HTMLElement} context
   *   An element to use as context for querySelectorAll.
   */
  Drupal.behaviors.i18n_sso.initLogin = (context) => {
    once('i18n_sso', drupalSettings.i18n_sso.wrapperSelector, context).forEach(
      (element) => {
        const div = document.createElement('div');
        div.setAttribute('id', 'sso-waiting');
        div.innerHTML = drupalSettings.i18n_sso.waiting;
        element.insertBefore(div, element.firstChild);
        Drupal.behaviors.i18n_sso.getToken();
      },
    );
  };

  /**
   * Retrieves token on configured url and calls the useToken function.
   */
  Drupal.behaviors.i18n_sso.getToken = () => {
    const xhr = new XMLHttpRequest();
    xhr.open('POST', drupalSettings.i18n_sso.ssoUrl);
    xhr.withCredentials = true;
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = () => {
      if (xhr.status === 200) {
        const data = JSON.parse(xhr.responseText);
        Drupal.behaviors.i18n_sso.useToken(data);
      }
    };
    xhr.send(`origin=${encodeURIComponent(window.location.origin)}`);
  };

  /**
   * Uses token on configured url.
   *
   * If result is "true", displays the message and reload the page.
   *
   * @param {object} data
   *   The data retrieved from the SSO URL endpoint.
   */
  Drupal.behaviors.i18n_sso.useToken = (data) => {
    const waitingElement = document.getElementById('sso-waiting');
    if (waitingElement) {
      waitingElement.innerHTML = data.message;
      if (data.token !== false) {
        const xhr = new XMLHttpRequest();
        xhr.open('POST', drupalSettings.i18n_sso.ssoLogin);
        xhr.setRequestHeader(
          'Content-Type',
          'application/x-www-form-urlencoded',
        );
        xhr.onload = () => {
          if (xhr.status === 200) {
            const loginData = JSON.parse(xhr.responseText);
            waitingElement.innerHTML = loginData.message;
            if (loginData.success === true) {
              window.location.reload();
            }
          }
        };
        xhr.send(`token=${encodeURIComponent(data.token)}`);
      }
    }
  };
})(Drupal, drupalSettings, once);
