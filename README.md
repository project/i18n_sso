# Internationalization Single Sign-On

This module is primarily intended for multilingual sites that detect the user
language via the URL and which utilize different top-level domains (TLDs).

You may have both `example.com` and `example.es`, for example.

Administration of such sites can often be frustrating, since cookie policies do
not allow example.es to read the session cookies set upon login to example.com.
Users and administrators must therefore log into each site separately, even
though the installation of Drupal is the same.

This module allows to automatically log the user in when getting a 403 error
page if he/she is logged in on the default language domain.

**How it works:**

On a website with the domains `example.com` and `example.es`. `example.com` is
the default language domain.

When getting access denied page on `example.es`, the module adds a JS file.

The JavaScript makes a first request on the endpoint
`example.com/i18n_sso/get-token`:

- if the user is not logged in on this domain, a message is displayed asking
  the user to log in on the default language domain (`example.com`).
- otherwise a token is retrieved or generated for this user and sent back in
  the response. The token lifetime is 10 minutes.

If a token is obtained, the JavaScript makes a second request on
`example.es/i18n_sso/login` with the token in parameter:

- if the token is still valid, the user is logged in and so a session cookie
  can be set on the `example.es` domain. And the page is reloaded.
- if the token is not valid anymore a message is displayed to inform the user
  that an error has occurred.

**Note on security:**

It is recommended to serve `i18n_sso/*` paths over HTTPS. That will provide extra
security.

**Note for developers:**

This module adds a JavaScript file on 403 pages to log the user in using ajax
requests. During the requests the JavaScript adds some markup on the page to
inform the visitor about the status of the process.

You can control the selector on which to append this markup by overriding
the selector in the services.yml file of your website. See the parameters
section of the [i18n_sso.services.yml](./i18n_sso.services.yml) file.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no settings or configuration.
